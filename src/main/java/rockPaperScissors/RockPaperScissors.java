package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

//import javax.swing.text.StyledEditorKit.BoldAction;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
       
        while (true) {
            System.out.println(String.format("Let's play round %d", roundCounter));
            String humanChoice = userChoice();
            String cpuChoice = randomChoice();
            String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, cpuChoice);   

            if (isWinner(humanChoice, cpuChoice)) {
                System.out.println(choiceString + " Human wins!");
                humanScore++;
            }
            else if (isWinner(cpuChoice, humanChoice)) {
                System.out.println(choiceString + " Computer wins!");
                computerScore++;
            }
            else {
                System.out.println(choiceString + " It's a tie");
            }
            System.out.println(String.format("Score: human %d, computer %d", computerScore, humanScore));
            roundCounter++;
            String continueAnswer = continuePlaying();
            if (continueAnswer.equals("n")) {
                System.out.println("Bye bye :)");
                return;
            }
        }
        
    

    }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    public String randomChoice() {
        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return randomElement;
    }
    public boolean isWinner(String choice1, String choice2) {
        // boolean boolVar;
        if (choice1 == "paper") {
            return (choice2 == "rock");
        }
        else if (choice1 == "scissors") {
            return (choice2 == "paper");
        }
        else {
            return (choice2 == "scissors");
        }
    }
    public String userChoice() {
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?"); 
            if (validateInput(humanChoice, rpsChoices)) {
                return humanChoice;
            }
            else {
                System.out.println(String.format("I dont understand %s. Could you try again?", humanChoice));
            }
        }
    }
    public boolean validateInput(String input, List<String> validInputs) {
        input = input.toLowerCase();
        boolean check = validInputs.contains(input);
        return check; 
    }
    public String continuePlaying() {
        
        while (true) {
            List<String> validCont = Arrays.asList("y","n");
            String continueVar = readInput("Do you wish to continue playing? (y/n)?");
            continueVar = continueVar.toLowerCase();
            if (validateInput(continueVar, validCont)) {
                return continueVar;
            }
            else {
                System.out.println(String.format("I don't understand %s. Could you try again?", continueVar));
            }

        }
    }


}
